#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# I won't say this is well written code, but it works
# I'll fix it if needed, but for now, quick'n'dirty is good enough

import socket, ssl
import datetime, re, random, time, os
import base64
import requests, json, io, csv
import traceback, signal
import selectors
import inotify
signal.signal(signal.SIGINT, signal.SIG_DFL)

def print_time(*args):
	print(datetime.datetime.now().strftime("[%H:%M:%S]"), *args, flush=True)

def send(line, log=True):
	if log:
		print_time("<<", line)
	else:
		print_time("<< (redacted)")
	assert '\n' not in line
	assert '\r' not in line
	assert '\0' not in line
	sock.send(line.encode("utf-8")+b"\n")

watchlist = {
	"#winehackers": [ ( 5, "wine" ), ( 231, "wine-staging" ), ( 1838, "FW mono" ), ( 1840, "wine-mono" ), ],
	"#vkd3d": [ ( 145, "vkd3d" ), ],
	}
next_commits_check = time.time()+1800

watched_repo_ids = []  # list[int]
watch_report_to = {}  # dict[int, list[str]]
watchlist_names = {}  # dict[int, str]
for ch,pairs in watchlist.items():
	for iid,name in pairs:
		if iid not in watched_repo_ids:
			watched_repo_ids.append(iid)
			watchlist_names[iid] = name
			watch_report_to[iid] = []
		watch_report_to[iid].append(ch)
watched_repo_ids = sorted(watched_repo_ids)

try:
	state = json.loads(open("state.json","rt").read())
except Exception:
	state = {}
latest_commits = state.get("latest_commits",{})
state["latest_commits"] = { repo : latest_commits.get(str(repo),None) for repo in watched_repo_ids }
state["merge_request_count"] = state.get("merge_request_count", 0)

latest_commits = [ None ] * len(watched_repo_ids)

c_bold = "\x02"
c_reset = "\x0F"
c_white = "\x0300"  # use with caution, it looks bad on white background
c_black = "\x0301"  # use with caution, it looks bad on black background
c_darkblue = "\x0302"
c_darkgreen = "\x0303"
c_red = "\x0304"
c_darkred = "\x0305"
c_purple = "\x0306"
c_orange = "\x0307"
c_yellow = "\x0308"  # use with caution, it looks bad on white background
c_green = "\x0309"
c_darkteal = "\x0310"
c_lightteal = "\x0311"
c_blue = "\x0312"  # use with caution, it looks bad on white background
c_pink = "\x0313"  # use with caution, it looks bad on white background
c_gray = "\x0314"
c_lightgray = "\x0315"  # use with caution, it looks bad on white background

last_fetch = {}
def get(url):
	class dummy:
		status_code = -1
		text = ""
	
	now = time.time()
	ago = now - last_fetch.get(url, 0.0)
	if ago < 5:
		time.sleep(5 - ago)
	last_fetch[url] = now
	
	try:
		# can throw requests.exceptions.ConnectionError (containing urllib3.exceptions.MaxRetryError,
		#  containing urllib3.exceptions.NewConnectionError, containing ConnectionRefusedError) if server is down
		return requests.get(url)
	except requests.exceptions.ConnectionError:
		print_time("failed.")
		return dummy()

def handle_message(text, chan):
	if text.startswith("!hr ") or text.startswith("!nts "):
		import hresult
		hr_val = hresult.parse_number(text[4:].strip())
		if hr_val is None:
			name = hresult.encode_hresult(text[4:].strip())
			if name:
				return name
			return "Unknown or invalid input"
		hr = hresult.decode_hresult(hr_val)
		if hr:
			return ', '.join(hr)
		else:
			if hr_val < 0x10000:
				hr = hresult.decode_hresult(hr_val | 0x80070000)
				if hr:
					return "Not found, but "+hex(hr_val|0x80070000)+" is "+', '.join(hr)
			return "Not found"
	if text.startswith("!gle "):
		import hresult
		try:
			gle_val = int(text[5:], 0)
		except ValueError:
			return "Decimal input only"
		if gle_val < 0:
			return "Decimal input only"
		if gle_val >= 0x10000:
			return "GetLastError doesn't go that high"
		gle = hresult.decode_hresult(gle_val | 0x80070000)
		if gle:
			return ', '.join(gle)
		else:
			return "Not found"
	
	if m := re.search(r"(?<!/)\b[0-9a-fA-F]{8,}\b", text):
		commit = m[0]
		for proj in watched_repo_ids:
			rsp = get("https://gitlab.winehq.org/api/v4/projects/"+str(proj)+"/repository/commits/"+commit)
			if rsp.status_code == 200:
				commit = json.loads(rsp.text)
				return commit["author_name"]+" * "+commit["short_id"]+" : "+commit["title"]+" "+commit["web_url"]
	
	if m := re.search(r"\b[Bb]ug ([0-9]+)\b", text):
		bug_id = m[1]
		text = get("https://bugs.winehq.org/buglist.cgi?columnlist=short_desc,bug_status,resolution&ctype=csv&bug_id="+bug_id).text
		bugs = list(csv.DictReader(io.StringIO(text)))
		if len(bugs):
			bug, = bugs
			# resolution can be "FIXED" "CLOSED" "ABANDONED", or " ---"
			# I have no idea why bugzilla is like that, it just is
			status = bug["bug_status"] + (" "+bug["resolution"]) * (bug["resolution"]!=" ---")
			return c_purple+"Bug "+bug["bug_id"]+c_reset+": ["+c_darkteal+status+c_reset+"] "+bug["short_desc"]+\
				" - "+c_gray+"https://bugs.winehq.org/show_bug.cgi?id="+bug["bug_id"]
		# if not, probably not a valid bug id; just ignore it
	
	if m := re.search(r"!([0-9]+)\b", text):
		mr_id = m[1]
		if chan not in watchlist:
			chan = next(iter(watchlist.keys()))
		rsp = get("https://gitlab.winehq.org/api/v4/projects/"+str(watchlist[chan][0][0])+"/merge_requests/"+mr_id)
		print_time("**", rsp.status_code)
		if rsp.status_code == 200:
			# IID is the only one that matters
			# ID is global across the gitlab instance I think; it shows up nowhere
			mr = json.loads(rsp.text)
			return "MR !"+str(mr["iid"])+": ["+mr["state"]+"] "+mr["title"]+\
				" - https://gitlab.winehq.org/wine/"+watchlist[chan][0][1]+"/-/merge_requests/"+str(mr["iid"])

def should_fetch(targets, target):
	if targets is None:
		return True
	if target in targets:
		return True
	return False

def check_for_new_commits_inner(targets=None):
	latest_commits = state["latest_commits"]
	ret = []
	for proj in watched_repo_ids:
		if not should_fetch(targets, ('commit',proj)):
			continue
		def try_print_latest(n):
			url = "https://gitlab.winehq.org/api/v4/projects/"+str(proj)+"/repository/commits?per_page="+str(n)
			print(url)
			rsp = get(url)
			if rsp.status_code != 200:
				return True
			commits = json.loads(rsp.text)
			print(commits)
			active = False
			for commit in commits[::-1]:
				if not active:
					if commit["id"] == latest_commits[proj]:
						active = True
					continue
				notice = commit["author_name"]+" * "+commit["short_id"]+" : "+commit["title"]
				notice = "["+watchlist_names[proj]+"] "+notice
				ret.append(( proj, notice ))
			if commits and not active:
				return False
			if commits:
				latest_commits[proj] = commits[0]["id"]
			return True
		if not try_print_latest(10):  # start out low, to not waste server resources
			if not try_print_latest(100):
				ret.append(( proj, "something weird happened. Send help" ))
	
	if ret:
		# so it doesn't print MRs right beside a wall of text
		return ret, True
	
	if should_fetch(targets, ('mr',)):
		url = "https://gitlab.winehq.org/api/v4/merge_requests?scope=all&per_page=5"
		print(url)
		rsp = get(url)
		if rsp.status_code == 200:
			mrs = json.loads(rsp.text)
			print(mrs)
			if state["merge_request_count"]:
				for mr in mrs[::-1]:
					if mr["id"] <= state["merge_request_count"]:
						continue
					proj = mr["project_id"]
					if proj not in watched_repo_ids:
						continue
					ret.append(( proj, "New MR: ["+watchlist_names[proj]+"] "+mr["title"]+" "+mr["web_url"] ))
			state["merge_request_count"] = mrs[0]["id"]
	
	with open("state.json","wt") as f:
		f.write(json.dumps(state))
	
	return ret, False

def check_for_new_commits(targets=None):
	notices, force_count = check_for_new_commits_inner(targets)
	out_notices = { ch : [] for ch in watchlist.keys() }
	for iid,n in notices:
		for ch in watch_report_to[iid]:
			out_notices[ch].append(n)
	for ch,ns in out_notices.items():
		for i,n in enumerate(ns):
			#if len(ns) >= 5 or force_count:
			#	n = "("+str(i+1)+"/"+str(len(notices))+") "+n
			send("PRIVMSG "+ch+" :"+n)
			if len(ns) >= 5:
				time.sleep(2)  # I'd replace this with https://ircv3.net/specs/extensions/multiline if it was implemented on Libera

sock = None
sel = selectors.DefaultSelector()
def disconnect():
	global sock
	try:
		sel.unregister(sock)
	except KeyError:
		# who cares. it's unregistered, why would I care if it was regged or not
		pass
	sock = None

line_bytes = b""
next_ping = 0
ping_sent = False
connected = False

maildir = "/var/mail/alcarobot/new/"
if os.path.isdir(maildir):
	ino = inotify.watcher()
	ino.watch_dir(maildir)
	sel.register(ino, selectors.EVENT_READ, None)
else:
	ino = None

def process_maildir():
	if ino is None:
		return
	targets = set()
	print_time("checking maildir")
	for fn in os.listdir(maildir):
		again = True
		print_time("email", fn)
		try:
			text = None
			text = open(maildir+fn,"rt").read()
			headers_raw,body = text.replace("\r","").split("\n\n",1)
			
			headers_lines = headers_raw.split("\n")
			i = len(headers_lines)-1
			while i >= 0:
				if i > 0 and headers_lines[i][0] in ' \t':
					headers_lines[i-1] += ' '+headers_lines.pop(i).strip()
				i -= 1
			
			headers = {}
			for line in headers_lines:
				k,v = line.split(":",1)
				headers[k] = v.strip()
			
			print_time("email contents", headers["To"], headers["X-GitLab-Project-Id"], headers["Subject"])
			
			proj = int(headers["X-GitLab-Project-Id"])
			if proj in watched_repo_ids:
				if headers["To"] == "wine-gitlab@winehq.org":
					# skip if it starts with Re (is a comment on something), is a v2, or is not the first mail in the series
					# (some patch series have a 0/ containing the MR message, but some don't)
					if headers["Subject"].startswith("[PATCH 1/"):
						targets.add(( "mr", ))
				elif headers["To"] == "wine-commits@winehq.org":
					targets.add(( "commit", proj ))
				else:
					1/0
		except Exception:
			print(text)
			traceback.print_exc()
		os.remove(maildir+fn)
	if targets:
		print_time("emails say", targets)
		check_for_new_commits(targets)

while True:
	try:
		if sock is None:
			print_time("** Connecting")
			
			sock_raw = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock_raw.settimeout(90)
			sock = ssl.create_default_context().wrap_socket(sock_raw, server_hostname="irc.libera.chat")
			sock.settimeout(90)
			sock.connect(("irc.libera.chat",6697))
			print_time("** Connected")
			ping_sent = False
			connected = False
			send("CAP REQ :sasl")
			send("AUTHENTICATE PLAIN")
			passwd = open("passwd.txt","rt").read()
			send("AUTHENTICATE "+base64.b64encode(('winebot\0winebot\0'+passwd).encode("utf-8")).decode("utf-8"), log=False)
			send("CAP END")
			# max length is 50
			send("USER winebot a a :https://gitlab.winehq.org/Alcaro/winebot")
			send("NICK winebot")
			send("MODE winebot +B")  # not implemented on the Libera server, but...
			sel.register(sock, selectors.EVENT_READ, None)
			next_ping = time.time()+90
		
		events = sel.select(timeout=min(next_ping+1-time.time(), 90))
		
		for ev,mask in events:
			if ev.fileobj is None:
				continue
			if ev.fileobj is sock:
				seg = sock.recv(4096)
				if seg == b'':
					print_time("Disconnected: EOF from server")
					disconnect()
					continue
				line_bytes += seg
				lines = line_bytes.split(b"\n")
				line_bytes = lines[-1]
				for line in lines[:-1]:
					ping_sent = False
					line = line.decode("utf-8", errors="replace").rstrip("\r")
					print_time(">>", line)
					
					src_full = ""
					if line.startswith(":"):
						src_full,line = line.split(" ",1)
						src_full = src_full[1:]
					words = []
					while True:
						if line.startswith(":"):
							words.append(line[1:])
							break
						elif ' ' in line:
							word,line = line.split(" ",1)
							words.append(word)
						else:
							words.append(line)
							break
					
					if words[0] == "PING":
						send("PONG :"+words[1])
					if words[0] == "001":
						send("JOIN "+','.join(watchlist.keys()))
						connected = True
						process_maildir()
					if words[0] == "433":
						newnick = "winebot"+f'{random.randint(0,9999):04d}'
						send("NICK "+newnick)
						send("MODE "+newnick+" +B")
					if words[0] == "PRIVMSG":
						dst = words[1]
						src = src_full.split("!")[0]
						if not dst.startswith("#"):
							dst = src
						
						reply = handle_message(words[2], dst)
						if reply:
							send("PRIVMSG "+dst+" :"+reply)
						if words[2] == "\1VERSION\1":
							send("NOTICE "+dst+" :\1VERSION Sir_Walrus proudly presents: "+
							                               "winebot <https://gitlab.winehq.org/Alcaro/winebot>\1")
			if ev.fileobj is ino:
				ino.read()  # discard that, iterate the entire directory
				if connected:
					process_maildir()
		
		if connected and time.time() >= next_ping:
			send("PING :1")
			if ping_sent:
				print_time("Disconnected: Ping timeout")
				disconnect()
				continue
			ping_sent = True
			next_ping = time.time()+90
		
		if connected and time.time() > next_commits_check:
			check_for_new_commits()
			next_commits_check = time.time()+1800
	
	except Exception:
		traceback.print_exc()
		try:
			send("QUIT :ERROR: bot crashed")
			time.sleep(3)
			sock.close()
		except Exception:
			pass
		disconnect()
		time.sleep(60)
