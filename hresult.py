#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later

import json, sys, os, re

current_dir = os.path.dirname(os.path.realpath(__file__))

try:
	in_hresults = json.loads(open(current_dir+"/hresults.json","rt").read())
	reverse_hresults = { n:v  for v,ns in in_hresults.items()  for n in ns }
except Exception:
	# hresults.json is not in git because it's not source code, better recreate it yourself
	# to do this, run ./hresult.py /path/path/wine-git/include/, it'll create hresults.json beside itself
	# with Wine version 4161e62e47 (Release 10.0-rc1), this returns 14281 names for 13787 different hresults
	pass

def parse_number(hr_text):
	try:
		if len(hr_text) > 8:
			hr_val = int(hr_text, 10)
			if hr_val < 0:
				hr_val += 1<<32
		else:
			hr_val = int(hr_text, 16)
		if 0 <= hr_val <= 0xFFFFFFFF:
			return hr_val
	except ValueError:
		pass
	return None

def decode_hresult(hr):
	hr = f'{hr:08x}'
	if hr in in_hresults:
		return in_hresults[hr]
	return []

def encode_hresult(name):
	return reverse_hresults.get(name)

class hr_map:
	def __init__(self):
		self._by_name = {}  # { str(name) : tuple(bool(confident), int(hresult)) }
	
	def add(self, name, val):
		# these two have different definitions in xact.h and xact3.h
		if name == "XACTENGINE_E_SEEKTIMEBEYONDWAVEEND":
			val = ( True, 0x8ac7001a )
		if name == "XACTENGINE_E_NOFRIENDLYNAMES":
			val = ( True, 0x8ac7001b )
		if name in self._by_name:
			assert self._by_name[name][1] == val[1]
			if not val[0]:
				return  # if input is not confident, ignore it, so a prior confident input for the same hr remains confident
		self._by_name[name] = val
	
	def change_val(self, name, val):
		self._by_name[name] = val
	
	def get_val(self, name):
		return self._by_name[name]
	def try_get_val(self, name):
		return self._by_name.get(name)

hresults = hr_map()
facilities = {
	# a few secret facilities not in winerror.h
	"_FACD3D": 2166,
	"_FACDD": 2166,  # yes, it's duplicate
	"FACILITY_XACTENGINE": 2759,
	"_FACDPV": 21,  # why not FACILITY_DPLAY?
	"_FACDXDIAG": 7,  # that's just FACILITY_WIN32
	"_DPN_FACILITY_CODE": 21,  # why is dplay like this
	"_FACDP": 2167,
	"_FACDS": 2168,
	"_DPNH_FACILITY_CODE": 21,  # why
	"_FACD3DXF": 2166,
	"FACILITY_HID_ERROR_CODE": 17,  # facility 17 is COMPLUS aka .net framework
	"_FACWINED3D": 2166,  # the wine folks felt like reusing this one apparently
}
bases = {}

def join_facility(fac1, fac2):
	fac1x = fac1 & 0xFFFF0000
	fac2x = fac2 & 0xFFFF0000
	assert fac1x == 0 or fac2x == 0 or fac1x == fac2x
	return fac1|fac2

def make_hresult(text, error, facility, outer_facility, tail=0):
	error_map = { 0:0, 1:0x80000000, 2:0xC0000000 }
	return parse_int(text, join_facility(outer_facility, error_map[error] | facilities[facility] << 16 | tail), confident=True)

# returns tuple [ confidence (bool), value (int) ]
def parse_int(text, facility=0, confident=False):
	if facility == 0x80070000:
		confident = True
	try:
		return ( confident, join_facility(int(text, 0), facility) )
	except ValueError:
		pass
	if m := re.match(r'\((\w+) ?\+ ?(\w+)\)', text):
		base_str = m[1]
		offset_str = m[2]
		if re.match(r'[0-9a-fx]+', base_str):
			offset_str, base_str = base_str, offset_str
		base = hresults.try_get_val(base_str)
		if base is None:
			base = ( False, bases[base_str] )
		base = ( base[0], base[1] + int(offset_str, 0) )
		# if not confident:
			# print(base_str)
			# confident = True
			# base = ( True, base[1] | 0x80070000 )
			# hresults.change_val(base_str, base)
		return parse_int(offset_str, join_facility(base[1], facility), base[0]|confident)
	elif m := re.match(r'([A-Z0-9_]+)\((.+)\)', text):
		if m[1] == "HRESULT_FROM_WIN32":
			return parse_int(m[2], join_facility(0x80070000, facility), True)
		elif m[1] == "_HRESULT_TYPEDEF_" or m[1] == "_ASF_HRESULT_TYPEDEF_":
			return parse_int(m[2], facility, True)
		elif m[1] == "_NDIS_ERROR_TYPEDEF_":
			return parse_int(m[2], facility, True)
		elif m[1] == "__MSABI_LONG":
			return parse_int(m[2], facility, confident)
		elif m[1] == "MS_SUCCESS_CODE":       return make_hresult(m[2], 0, "FACILITY_ITF", facility)
		elif m[1] == "MS_ERROR_CODE":         return make_hresult(m[2], 1, "FACILITY_ITF", facility)
		elif m[1] == "MAKE_D3DHRESULT":       return make_hresult(m[2], 1, "_FACD3D", facility)
		elif m[1] == "MAKE_DDHRESULT":        return make_hresult(m[2], 1, "_FACDD", facility)
		elif m[1] == "MAKE_DVHRESULT":        return make_hresult(m[2], 1, "_FACDPV", facility)
		elif m[1] == "MAKE_AVIERR":           return make_hresult(m[2], 1, "FACILITY_ITF", facility, 0x4000)
		elif m[1] == "MAKE_DMHRESULTSUCCESS": return make_hresult(m[2], 0, "FACILITY_DIRECTMUSIC", facility, 0x1000)
		elif m[1] == "MAKE_DMHRESULTERROR":   return make_hresult(m[2], 1, "FACILITY_DIRECTMUSIC", facility, 0x1000)
		elif m[1] == "XACTENGINEERROR":       return make_hresult(m[2], 1, "FACILITY_XACTENGINE", facility)
		elif m[1] == "AUDCLNT_ERR":           return make_hresult(m[2], 1, "FACILITY_AUDCLNT", facility)
		elif m[1] == "RTWQ_E_ERROR":          return make_hresult(m[2], 2, "FACILITY_MEDIASERVER", facility)  # weird facility...
		elif m[1] == "HR_S":                  return make_hresult(m[2], 0, "FACILITY_INTERNET", facility)  # weird name...
		elif m[1] == "HR_E":                  return make_hresult(m[2], 1, "FACILITY_INTERNET", facility)
		elif m[1] == "SMAKEHR":               return make_hresult(m[2], 0, "FACILITY_URT", facility)
		elif m[1] == "EMAKEHR":               return make_hresult(m[2], 1, "FACILITY_URT", facility)
		elif m[1] == "STD_CTL_SCODE":         return make_hresult(m[2], 0, "FACILITY_CONTROL", facility)
		elif m[1] == "CUSTOM_CTL_SCODE":      return make_hresult(m[2], 0, "FACILITY_CONTROL", facility)
		elif m[1] == "MAKE_DXDIAGHRESULT":    return make_hresult(m[2], 1, "_FACDXDIAG", facility)
		elif m[1] == "MAKE_DPNHRESULT":       return make_hresult(m[2], 1, "_DPN_FACILITY_CODE", facility, 0x8000)
		elif m[1] == "MAKE_DPHRESULT":        return make_hresult(m[2], 1, "_FACDP", facility)
		elif m[1] == "AUDCLNT_SUCCESS":       return make_hresult(m[2], 0, "FACILITY_AUDCLNT", facility)
		elif m[1] == "MAKE_DSHRESULT":        return make_hresult(m[2], 1, "_FACDS", facility)
		elif m[1] == "MAKE_DPNHFAILURE":      return make_hresult(m[2], 1, "_DPNH_FACILITY_CODE", facility, 0xF000)
		elif m[1] == "MAKE_WINED3DHRESULT":   return make_hresult(m[2], 1, "_FACWINED3D", facility)
		elif m[1] == "MAKE_WINED3DSTATUS":    return make_hresult(m[2], 0, "_FACWINED3D", facility)
		elif m[1] == "MAKE_D3DSTATUS":        return make_hresult(m[2], 0, "_FACD3D", facility)
		elif m[1] == "HIDP_ERROR_CODES":
			m = re.match(r'(0x.)u,(0x..)', m[2])
			ret = make_hresult(m[2], 0, "FACILITY_HID_ERROR_CODE", facility)
			return ( ret[0], ret[1] | int(m[1],0)<<28 )
		elif m[1] == "MAKE_HRESULT" or m[1] == "MAKE_SCODE":
			m = re.match(r' ?(SEVERITY_[A-Z]+|0|1), *([A-Z0-9_]*FAC[A-Z0-9_]+), *(.+)', m[2])
			if m[1] == "SEVERITY_SUCCESS" or m[1] == "0":
				sev = 0
			elif m[1] == "SEVERITY_ERROR" or m[1] == "1":
				sev = 1
			else:
				1/0
			return make_hresult(m[3], sev, m[2], facility)
		else:
			int(m[1])
	elif text.startswith("((DWORD)") and text.endswith(")"):
		return parse_int(text[8:-1].strip(), facility, confident)
	elif text.startswith("(APPLICATION_ERROR_MASK|ERROR_SEVERITY_ERROR|") and text.endswith(")"):
		return parse_int(text[45:-1].strip(), 0xE0000000, True)
	elif text.startswith("((SCODE)") and text.endswith(")"):
		return parse_int(text[8:-1].strip(), facility, True)
	elif text.startswith("((HRESULT)") and text.endswith(")"):
		return parse_int(text[10:-1].strip(), facility, True)
	elif text.startswith("((NTSTATUS)") and text.endswith(")"):
		return parse_int(text[11:-1].strip(), facility, True)
	elif text.startswith("(") and text.endswith(")"):
		return parse_int(text[1:-1], facility, confident)
	elif text[0] in "0123456789" and text.endswith("L"):
		return ( confident, int(text[:-1], 0) )
	else:
		conf,val = hresults.get_val(text)
		return ( conf|confident, val )
	1/0

def compile_header(fn, text):
	text = text.replace("\\\n", "")
	for df in re.finditer(r"^#[ \t]*define[ \t]+([A-Za-z0-9_]+)[ \t]+(.*)$", text, re.MULTILINE):
		if fn.endswith("winerror.h"):
			if df[1] == "FACILITY_NT_BIT" or df[1].startswith("SEVERITY_"):
				pass
			elif df[1].startswith("FACILITY"):
				if df[1] == 'FACILITY_SECURITY':
					facilities[df[1]] = facilities[df[2]]
				else:
					facilities[df[1]] = int(df[2])
			else:
				try:
					if 'WSABASEERR' not in df[2]:
						int(df[2])
					yield ( 0x80070000, df[1], df[2], df[0] )
				except ValueError:
					yield ( 0, df[1], df[2], df[0] )
		else:
			if df[1] in (
					"SP_ERROR", "GDI_ERROR", "HGDI_ERROR", "MB_ICONERROR", "IDI_ERROR",
					"DNS_RCODE_FORMAT_ERROR", "DNS_RCODE_NAME_ERROR", "IMM_ERROR_NODATA", "IMM_ERROR_GENERAL", "SOCKET_ERROR",
					"E_ERRORACCESSINGFONTDATA", "E_ERRORACCESSINGFACENAME", "PT_ERROR",
					"TBLSTAT_SORT_ERROR", "TBLSTAT_SETCOL_ERROR", "TBLSTAT_RESTRICT_ERROR", "TABLE_ERROR",
					"MCIWNDM_GETERRORA", "MCIWNDM_GETERRORW", "MCIWNDM_GETERROR", "MCIWNDM_NOTIFYERROR",
					"WM_CAP_SET_CALLBACK_ERRORA", "WM_CAP_SET_CALLBACK_ERRORW", "WM_CAP_SET_CALLBACK_ERROR",
					"FSCTL_GET_RETRIEVAL_POINTER_BASE", "FSCTL_QUERY_DIRECT_IMAGE_ORIGINAL_BASE",
					"WINHTTP_CALLBACK_FLAG_ALL_COMPLETIONS", "RPC_BAD_STUB_DATA_EXCEPTION_FILTER", "CERT_DSS_SIGNATURE_LEN",
					"HID_USAGE_DIGITIZER_X_TILT", "HID_USAGE_LED_ERROR", "SNMPAPI_NOERROR", "SNMPAPI_ERROR", "LBER_ERROR",
					"TD_ERROR_ICON", "STDDISPID_XOBJ_ERRORUPDATE", "RL_RANGE_ERROR", "MAXERRORLENGTH", "TIMERR_NOERROR", "JOYERR_NOERROR",
					"WINE_IHTMLELEMENT_DISPINTERFACE_DECL", "WINE_IHTMLELEMENT6_DISPINTERFACE_DECL", "JOB_OUTPUT_FLAGS",
					"WM_SYSTEMERROR", "OIC_ERROR", "XTYP_ERROR", "HFILE_ERROR", "FOF_NO_UI", "NCM_DISPLAYERRORTIP", "_NLSCMPERROR",
					"PID_USAGE_BLOCK_LOAD_ERROR", "HFILE_ERROR16", 'ERROR_PATCH_TARGET_NOT_FOUND',
					'STATUS_SEVERITY_ERROR', 'D3DSETSTATUS_ALL', 'WINHTTP_INVALID_STATUS_CALLBACK', 'MAPI_STATUS', 'ICM_SET_STATUS_PROC',
					'INTERNET_INVALID_STATUS_CALLBACK', 'szOID_CMC_STATUS_INFO', 'CMC_STATUS', 'DPL_MSGID_SESSION_STATUS',
					'EM_GETCTFOPENSTATUS', 'EM_SETCTFOPENSTATUS', 'VSCLASS_STATUSSTYLE', 'VSCLASS_STATUS', 'STATUSSTYLEPARTS',
					'DAT_STATUS', 'MSG_CHECKSTATUS', 'TWRC_FAILURE', 'TWRC_CHECKSTATUS', 'STATUSCLASSNAMEA', 'STATUSCLASSNAMEW',
					'STATUSCLASSNAME', 'SC_MANAGER_ALL_ACCESS', 'ACTRL_SVC_STATUS', 'STDPROPID_XOBJ_STATUSBARTEXT',
					'DRVM_MAPPER_STATUS', 'SERVICE_ALL_ACCESS', 'WODM_MAPPER_STATUS', 'WIDM_MAPPER_STATUS', 'FCIDM_STATUS',
					'BFFM_SETSTATUSTEXTA', 'BFFM_SETSTATUSTEXTW', 'BFFM_SETSTATUSTEXT', 'TS_AS_ALL_SINKS', 'NET_API_STATUS',
					'API_RET_TYPE', 'SZDDESYS_ITEM_STATUS', 'DDE_FACKRESERVED', 'PR_SPOOLER_STATUS', 'PR_TRANSPORT_STATUS',
					'PR_RECIPIENT_STATUS', 'PR_MSG_STATUS', 'PR_STATUS', 'PR_STATUS_CODE', 'PR_STATUS_STRING_W', 'PR_STATUS_STRING_A',
					'PR_STATUS_STRING', 'PK_STATUS', 'NDIS_STATUS_FAILURE', 'PID_USAGE_BLOCK_LOAD_STATUS', 'THREAD_PRIORITY_ERROR_RETURN',
					'PS_ATTRIBUTE_ERROR_MODE',
					):
				continue
			if any(df[1].startswith(k) for k in (
					'OLEUI_', 'SP_PROT_', 'TWCC_', 'DISPID_', 'CMC_E', 'D3DVS_', 'SPLREG_', 'MAPI_E_', 'LZERROR_', 'DMLERR_',
					'SE_ERR_', 'PHCM_ERROR_', 'USBD_STATUS_', 'VK_KHR_', 'HTTP_STATUS_', 'D3DSTATUS_', 'WINHTTP_CALLBACK_FLAG_', 'WM_CAP_',
					'IOCTL_', 'STATUS_SEVERITY_', 'OID_802_', 'LINEERR_', 'DIAXIS_', )):
				continue
			if any(k in df[0] for k in ( '_S_', '_E_', '_X_', 'HRESULT(', 'HRESULT_', 'ERROR', 'ERR_', 'HR_', 'STATUS' )):
				yield ( 0, df[1], df[2], df[0] )
			elif 'BASE' in df[1]:
				try:
					bases[df[1]] = int(df[2], 0)
				except ValueError:
					pass

if __name__ == "__main__":
	try:
		print(decode_hresult(parse_number(sys.argv[1])))
		exit(0)
	except ValueError:
		pass
	
	nodes = []
	for fn in sys.argv[1:]:
		if os.path.isdir(fn):
			for dn,dirs,fns in os.walk(fn):
				for fn in fns:
					nodes.extend(compile_header(dn+"/"+fn, open(dn+"/"+fn).read()))
		else:
			nodes.extend(compile_header(fn, open(fn).read()))
	rounds = 0
	while nodes:
		success = False
		bad_nodes = []
		for facility,name,val,orig in nodes:
			try:
				val2 = parse_int(val, facility)
				hresults.add(name, val2)
				success = True
			except Exception:
				bad_nodes.append(( facility, name, val, orig ))
				# if rounds >= 2:
					# raise
		nodes = bad_nodes
		rounds += 1
		if not success:
			for n in nodes[:10]: print(n)
			print(len(hresults._by_name), len(nodes))
			print(parse_int(nodes[0][2], nodes[0][0]))
			break
	
	by_val = {}
	for name,(confident,val) in sorted(hresults._by_name.items(), key=lambda n__c_v: ( n__c_v[1][1] )):
		if val not in by_val:
			by_val[val] = []
		by_val[val].append(( confident, name ))
	for k in by_val.keys():
		vals = by_val[k]
		if k < 0x00040000  or  k&(k-1) == 0  or  0 < sum(v[0] for v in vals) < len(vals):
			vals = [ c_n for c_n in vals if c_n[0] ]
			if not vals:
				by_val[k] = []
				continue
		vals = [ c_n[1] for c_n in vals ]
		if any(n.startswith("ERROR_") for n in vals):
			vals = [ n for n in vals if n.startswith("ERROR_") or n.startswith("STATUS_") ]
		if any(n.startswith("E_") for n in vals):
			vals = [ n for n in vals if n.startswith("E_") or n.startswith("STATUS_") ]
		vals = [ v for v in vals if (not v.startswith("WINE") or v[4:] not in vals) ]
		by_val[k] = vals
	by_val = { k:v for k,v in by_val.items() if v }
	by_val[0] = [ "S_OK" ]
	by_val[1] = [ "S_FALSE" ]
	del by_val[4294967295]
	by_val[0xE06D7363] = [ "EXCEPTION_WINE_CXX_EXCEPTION" ]
	by_val = { f'{k:08x}' : v for k,v in by_val.items() }
	names = [ n  for ns in by_val.values()  for n in ns ]
	with open(current_dir+"/hresults.json","wt") as f:
		f.write(json.dumps(by_val))
	print("Discovered", len(names), "names for", len(by_val), "different hresults")
	# print(by_val)
