import ctypes, os, struct

class watcher:
	def __init__(self):
		libc = ctypes.CDLL("libc.so.6")
		self.fd = libc.inotify_init1(os.O_CLOEXEC|os.O_NONBLOCK)
		self.inotify_add_watch = libc.inotify_add_watch
	
	def watch_dir(self, fn):
		# just assume these guys aren't platform dependent
		IN_ACCESS        = 0x0001
		IN_MODIFY        = 0x0002
		IN_ATTRIB        = 0x0004
		IN_CLOSE_WRITE   = 0x0008
		IN_CLOSE_NOWRITE = 0x0010
		IN_OPEN          = 0x0020
		IN_MOVED_FROM    = 0x0040
		IN_MOVED_TO      = 0x0080
		IN_CREATE        = 0x0100
		IN_DELETE        = 0x0200
		IN_DELETE_SELF   = 0x0400
		IN_MOVE_SELF     = 0x0800
		
		flags = IN_MODIFY | IN_ATTRIB | IN_MOVED_FROM | IN_MOVED_TO | IN_CREATE | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF
		self.inotify_add_watch(self.fd, os.fsencode(fn), flags)
	
	def fileno(self):
		return self.fd
	
	def read(self):
		data = os.read(self.fd, 4096)
		pos = 0
		events = []
		while pos < len(data):
			wd, mask, cookie, namesize = struct.unpack_from("=iIII", data, pos)
			pos += 16 + namesize
			name = data[pos - namesize : pos]
			events.append(( wd, mask, cookie, os.fsdecode(name.rstrip(b"\x00")) ))
		return events
